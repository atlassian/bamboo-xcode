package com.atlassian.bamboo.plugins.xcode.testflight;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.logger.ErrorUpdateHandler;
import com.atlassian.bamboo.plugins.xcode.MockWebServerMultiPartsAware;
import com.atlassian.bamboo.plugins.xcode.TestResourcesHandler;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import okhttp3.mockwebserver.MockResponse;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.mail.MessagingException;
import javax.mail.internet.ParseException;
import java.io.IOException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class TestFlightTaskTest {

    @Rule
    public final MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    private MockWebServerMultiPartsAware mockWebServerMultiPartsAware;

    private TestFlightTask sut;

    @Mock
    private ErrorUpdateHandler errorUpdateHandler;

    @Before
    public void setup() throws IOException, ParseException {
        sut = new TestFlightTask(errorUpdateHandler);

        mockWebServerMultiPartsAware = new MockWebServerMultiPartsAware();
        mockWebServerMultiPartsAware.start();
    }

    @After
    public void teardown() {
        mockWebServerMultiPartsAware.close();
    }

    @Test
    public void testHttpCommunicationSuccess() throws TaskException, IOException, InterruptedException, MessagingException {
        // Given... server will respond the request with SUCCESS
        mockWebServerMultiPartsAware.enqueue(new MockResponse()
                .setBody(TestResourcesHandler.getResourceContents("testflight/response-success.json"))
                .setResponseCode(HttpStatus.SC_OK));

        // Given... configurations
        ConfigurationMap configurationMap = Mockito.mock(ConfigurationMap.class);
        when(configurationMap.get(TestFlightTaskConfigurator.FILE)).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.ipa"));
        when(configurationMap.get(TestFlightTaskConfigurator.DSYM)).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.dsym"));
        when(configurationMap.get(TestFlightTaskConfigurator.API_TOKEN)).thenReturn("my-api-token");
        when(configurationMap.get(TestFlightTaskConfigurator.TEAM_TOKEN)).thenReturn("my-team-token");
        when(configurationMap.get(TestFlightTaskConfigurator.NOTES)).thenReturn("my notes");
        when(configurationMap.getAsBoolean(TestFlightTaskConfigurator.NOTIFY)).thenReturn(Boolean.TRUE);
        when(configurationMap.get(TestFlightTaskConfigurator.DISTRIBUTION_LISTS)).thenReturn("list1,list2,list3");

        // Given... fake BuildLogger
        BuildLogger buildLogger = Mockito.mock(BuildLogger.class);

        // Given... the context to access the fake data created above
        CommonTaskContext commonTaskContext = Mockito.mock(CommonTaskContext.class);
        when(commonTaskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(commonTaskContext.getBuildLogger()).thenReturn(buildLogger);

        // When...
        TaskResult actualWasSuccess = sut.execute(commonTaskContext, mockWebServerMultiPartsAware.url("/"));

        // Then... successfully processed
        assertThat(actualWasSuccess.getTaskState(), is(TaskState.SUCCESS));

        // Then... successful response from server should have been logged
        Mockito.verify(buildLogger, Mockito.times(1)).addBuildLogEntry(ArgumentMatchers.contains("Upload completed"));

        // Then... the POST request should have been received by the server
        mockWebServerMultiPartsAware.takeRequest();
        assertThat(mockWebServerMultiPartsAware.getReceivedRequestMethod(), is("POST"));

        // Then... receivedParts are correctly received by the server
        final Map<String, String> receivedParts = mockWebServerMultiPartsAware.getRequestParts();

        assertThat(receivedParts.get(TestFlightTaskConfigurator.FILE), is( TestResourcesHandler.getResourceContents("my-app.ipa") ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.DSYM), is( TestResourcesHandler.getResourceContents("my-app.dsym") ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.API_TOKEN), is( "my-api-token"));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.TEAM_TOKEN), is("my-team-token"));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.NOTES), is( "my notes" ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.NOTIFY), is( "true" ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.DISTRIBUTION_LISTS), is( "list1,list2,list3" ));
    }

    @Test
    public void testHttpCommunicationFailure() throws TaskException, IOException, InterruptedException, MessagingException {
        // Given... server will respond the request with ERROR and some extra info in the body
        final String serverErrorMessage = "Internal error";
        mockWebServerMultiPartsAware.enqueue(new MockResponse()
                .setResponseCode(HttpStatus.SC_INTERNAL_SERVER_ERROR));

        // Given... VALID configurations
        ConfigurationMap configurationMap = Mockito.mock(ConfigurationMap.class);
        when(configurationMap.get(TestFlightTaskConfigurator.FILE)).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.ipa"));
        when(configurationMap.get(TestFlightTaskConfigurator.DSYM)).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.dsym"));
        when(configurationMap.get(TestFlightTaskConfigurator.API_TOKEN)).thenReturn("my-api-token");
        when(configurationMap.get(TestFlightTaskConfigurator.TEAM_TOKEN)).thenReturn("my-team-token");
        when(configurationMap.get(TestFlightTaskConfigurator.NOTES)).thenReturn("my notes");
        when(configurationMap.getAsBoolean(TestFlightTaskConfigurator.NOTIFY)).thenReturn(Boolean.TRUE);
        when(configurationMap.get(TestFlightTaskConfigurator.DISTRIBUTION_LISTS)).thenReturn("list1,list2,list3");

        // Given... fake BuildLogger
        BuildLogger buildLogger = Mockito.mock(BuildLogger.class);

        // Given... the context to access the fake data created above
        CommonTaskContext commonTaskContext = Mockito.mock(CommonTaskContext.class);
        when(commonTaskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(commonTaskContext.getBuildLogger()).thenReturn(buildLogger);

        // When...
        TaskResult actualWasSuccess = sut.execute(commonTaskContext, mockWebServerMultiPartsAware.url("/"));

        // Then... successfully processed
        assertThat(actualWasSuccess.getTaskState(), is(TaskState.FAILED));

        // Then... error response from server should have been logged
        Mockito.verify(buildLogger, Mockito.times(1)).addErrorLogEntry(ArgumentMatchers.matches("POST response to .* was " + HttpStatus.SC_INTERNAL_SERVER_ERROR));

        // Then... the POST request should have been received by the server
        mockWebServerMultiPartsAware.takeRequest();
        assertThat(mockWebServerMultiPartsAware.getReceivedRequestMethod(), is("POST"));

        // Then... receivedParts are correctly received by the server despite the expected error
        final Map<String, String> receivedParts = mockWebServerMultiPartsAware.getRequestParts();

        assertThat(receivedParts.get(TestFlightTaskConfigurator.FILE), is( TestResourcesHandler.getResourceContents("my-app.ipa") ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.DSYM), is( TestResourcesHandler.getResourceContents("my-app.dsym") ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.API_TOKEN), is( "my-api-token"));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.TEAM_TOKEN), is("my-team-token"));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.NOTES), is( "my notes" ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.NOTIFY), is( "true" ));
        assertThat(receivedParts.get(TestFlightTaskConfigurator.DISTRIBUTION_LISTS), is( "list1,list2,list3" ));
    }
}
