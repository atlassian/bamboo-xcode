package com.atlassian.bamboo.plugins.xcode;

import com.google.common.io.CharStreams;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.apache.commons.fileupload.MultipartStream;
import org.jetbrains.annotations.NotNull;

import javax.mail.internet.ContentDisposition;
import javax.mail.internet.ParseException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A wrapper for MockWebServer that helps to handle multipart content
 */
public class MockWebServerMultiPartsAware implements AutoCloseable {

    private static final int MULTIPART_STREAM_BUFFER_SIZE = 4096;

    private final MockWebServer mockWebServer;

    private RecordedRequest currentRequestTaken;

    public MockWebServerMultiPartsAware() {
        this.mockWebServer = new MockWebServer();
    }

    public MockWebServer getMockWebServer() {
        return mockWebServer;
    }

    public String url(final String path) {
        return mockWebServer.url("/").toString();
    }

    public String getReceivedRequestMethod() {
        return currentRequestTaken.getMethod();
    }

    public RecordedRequest takeRequest() throws InterruptedException {
        this.currentRequestTaken = mockWebServer.takeRequest();
        return currentRequestTaken;
    }

    public Map<String, String> getRequestParts() throws IOException, ParseException {

        final Map<String, String> parts = new HashMap<>();

        final MultipartStream multipartStream = new MultipartStream(
                currentRequestTaken.getBody().inputStream(),
                getMultiPartsBoundary(currentRequestTaken),
                MULTIPART_STREAM_BUFFER_SIZE,
                null);

        boolean nextPart = multipartStream.skipPreamble();
        while(nextPart) {
            parts.put( getPartName(multipartStream), getPartContents(multipartStream) );
            nextPart = multipartStream.readBoundary();
        }

        return parts;
    }

    /**
     * Parse a string containing HTTP headers and return a map from its name to its value.
     *
     * @param rawHeaders the list of strings with the headers
     * @return header name to header value map
     */
    private static Map<String, List<String>> parseHeaders(List<String> rawHeaders) throws IOException {
        final Map<String, List<String>> headers = new HashMap<>();
        for (String rawHeader : rawHeaders) {
            if (rawHeader.isEmpty()) {
                continue;
            }
            String headerName;
            String headerValue = null;
            int separatorPosition = rawHeader.indexOf(':');
            if (separatorPosition > 0) {
                headerName = rawHeader.substring(0, separatorPosition).trim();
                headerValue = rawHeader.substring(separatorPosition + 1).trim();
            } else {
                headerName = rawHeader.trim();
            }
            List<String> currentValues = headers.getOrDefault(headerName, new LinkedList<>());
            if (headerValue != null) {
                currentValues.add(headerValue);
            }
            headers.put(headerName, currentValues);
        }
        return headers;
    }

    private static String getPartContents(MultipartStream multipartStream) throws IOException {
        final OutputStream bodyDataStream = new ByteArrayOutputStream();
        multipartStream.readBodyData(bodyDataStream);
        return bodyDataStream.toString();
    }

    private static String getPartName(MultipartStream multipartStream) throws IOException, ParseException {
        final Map<String, List<String>> partHeaders = parseHeaders(CharStreams.readLines(new StringReader(multipartStream.readHeaders())));
        ContentDisposition contentDispositionHeader = new ContentDisposition(partHeaders.get("Content-Disposition").get(0));
        return contentDispositionHeader.getParameter("name");
    }

    @NotNull
    private static byte[] getMultiPartsBoundary(@NotNull RecordedRequest recordedRequest) {
        int boundaryIndex = Objects.requireNonNull(recordedRequest.getHeader("Content-Type"))
                .indexOf("boundary=");
        return (Objects.requireNonNull(recordedRequest.getHeader("Content-Type"))
                .substring(boundaryIndex + 9)).getBytes();
    }

    @Override
    public void close() {
        try {
            mockWebServer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() throws IOException {
        mockWebServer.start();
    }

    public void enqueue(MockResponse mockResponse) {
        mockWebServer.enqueue(mockResponse);
    }
}
