package com.atlassian.bamboo.plugins.xcode.hockeyapp;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.xcode.MockWebServerMultiPartsAware;
import com.atlassian.bamboo.plugins.xcode.TestResourcesHandler;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import okhttp3.mockwebserver.MockResponse;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.mail.MessagingException;
import javax.mail.internet.ParseException;
import java.io.IOException;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class UploadTaskTest {

    @Rule
    public final MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    private MockWebServerMultiPartsAware mockWebServerMultiPartsAware;

    private UploadTask sut;

    @Before
    public void setup() throws IOException, ParseException {
        sut = new UploadTask();

        mockWebServerMultiPartsAware = new MockWebServerMultiPartsAware();
        mockWebServerMultiPartsAware.start();
    }

    @After
    public void teardown() {
        mockWebServerMultiPartsAware.close();
    }

    @Test
    public void testHttpCommunicationSuccess() throws TaskException, IOException, InterruptedException, MessagingException {
        // Given... server will respond the request with SUCCESS
        mockWebServerMultiPartsAware.enqueue(new MockResponse()
                .setResponseCode(HttpStatus.SC_CREATED));

        // Given... configurations
        ConfigurationMap configurationMap = Mockito.mock(ConfigurationMap.class);
        when(configurationMap.get("apitoken")).thenReturn("my-api-token");
        when(configurationMap.get("ipa")).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.ipa"));
        when(configurationMap.get("dsym")).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.dsym"));
        when(configurationMap.get("notes")).thenReturn("my notes");
        when(configurationMap.get("download")).thenReturn("true");
        when(configurationMap.get("tags")).thenReturn("test");
        when(configurationMap.get("notify")).thenReturn("true");

        // Given... fake BuildLogger
        BuildLogger buildLogger = Mockito.mock(BuildLogger.class);

        // Given... the context to access the fake data created above
        CommonTaskContext commonTaskContext = Mockito.mock(CommonTaskContext.class);
        when(commonTaskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(commonTaskContext.getBuildLogger()).thenReturn(buildLogger);

        // When...
        TaskResult actualWasSuccess = sut.execute(commonTaskContext, mockWebServerMultiPartsAware.url("/"));

        // Then... successfully processed
        assertThat(actualWasSuccess.getTaskState(), is(TaskState.SUCCESS));

        // Then... successful response from server should have been logged
        Mockito.verify(buildLogger, Mockito.times(1)).addBuildLogEntry(ArgumentMatchers.contains("Upload completed!"));

        // Then... the POST request should have been received by the server
        mockWebServerMultiPartsAware.takeRequest();
        assertThat(mockWebServerMultiPartsAware.getReceivedRequestMethod(), is("POST"));

        // Then... receivedParts are correctly received by the server
        final Map<String, String> receivedParts = mockWebServerMultiPartsAware.getRequestParts();
        assertThat(receivedParts.get("ipa"), is( TestResourcesHandler.getResourceContents("my-app.ipa") ));
        assertThat(receivedParts.get("dsym"), is( TestResourcesHandler.getResourceContents("my-app.dsym") ));
        assertThat(receivedParts.get("notes"), is( "my notes" ));
        assertThat("Derived status from download=true should be 2", receivedParts.get("status"), is( "2" ));
        assertThat(receivedParts.get("tags"), is( "test" ));
        assertThat(receivedParts.get("notify"), is( "1" ));

        // Then... Monitoring should have tracked the request
        Mockito.verify(buildLogger, Mockito.atLeastOnce()).addBuildLogEntry(ArgumentMatchers.startsWith("Upload progress:"));
    }

    @Test
    public void testHttpCommunicationFailure() throws TaskException, IOException, InterruptedException, MessagingException {
        // Given... server will respond the request with ERROR and some extra info in the body
        final String serverErrorMessage = "Internal error";
        mockWebServerMultiPartsAware.enqueue(new MockResponse()
                .setBody(serverErrorMessage)
                .setResponseCode(HttpStatus.SC_INTERNAL_SERVER_ERROR));

        // Given... VALID configurations
        ConfigurationMap configurationMap = Mockito.mock(ConfigurationMap.class);
        when(configurationMap.get("apitoken")).thenReturn("my-api-token");
        when(configurationMap.get("ipa")).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.ipa"));
        when(configurationMap.get("dsym")).thenReturn(TestResourcesHandler.getResourceAbsolutePath("my-app.dsym"));
        when(configurationMap.get("notes")).thenReturn("my notes");
        when(configurationMap.get("download")).thenReturn("true");
        when(configurationMap.get("tags")).thenReturn("test");
        when(configurationMap.get("notify")).thenReturn("true");

        // Given... fake BuildLogger
        BuildLogger buildLogger = Mockito.mock(BuildLogger.class);

        // Given... the context to access the fake data created above
        CommonTaskContext commonTaskContext = Mockito.mock(CommonTaskContext.class);
        when(commonTaskContext.getConfigurationMap()).thenReturn(configurationMap);
        when(commonTaskContext.getBuildLogger()).thenReturn(buildLogger);

        // When...
        TaskResult actualWasSuccess = sut.execute(commonTaskContext, mockWebServerMultiPartsAware.url("/"));

        // Then... processed with error
        assertThat(actualWasSuccess.getTaskState(), is(TaskState.FAILED));

        // Then... Error should have been tracked by buildLogger
        Mockito.verify(buildLogger, Mockito.times(1)).addErrorLogEntry(ArgumentMatchers.contains("Upload failed with response:"));
        Mockito.verify(buildLogger, Mockito.times(1)).addErrorLogEntry(serverErrorMessage);

        // Then... the POST request should have been received by the server despite the expected response error
        mockWebServerMultiPartsAware.takeRequest();
        assertThat(mockWebServerMultiPartsAware.getReceivedRequestMethod(), is("POST"));

        // Then... receivedParts are correctly received by the server despite the expected error response
        final Map<String, String> receivedParts = mockWebServerMultiPartsAware.getRequestParts();
        assertThat(receivedParts.get("ipa"), is( TestResourcesHandler.getResourceContents("my-app.ipa") ));
        assertThat(receivedParts.get("dsym"), is( TestResourcesHandler.getResourceContents("my-app.dsym") ));
        assertThat(receivedParts.get("notes"), is( "my notes" ));
        assertThat("Derived status from download=true should be 2", receivedParts.get("status"), is( "2" ));
        assertThat(receivedParts.get("tags"), is( "test" ));
        assertThat(receivedParts.get("notify"), is( "1" ));
    }


}
