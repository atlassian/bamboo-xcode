package com.atlassian.bamboo.plugins.xcode;

import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Helps to handle test resources
 */
public class TestResourcesHandler {

    /**
     * Provide the absolute path for a resource
     * @param resourceName  the name of the resource, including the project-relative path where it can be found.
     *                     E.g.: if resource is {project}/test/resource/my-file.txt, then "my-file.txt" must be provided.
     * @return the absolute path of the given resource
     */
    @NotNull
    public static String getResourceAbsolutePath(final String resourceName) {
        return new File(
                getResourceUrl(resourceName).getFile())
                .getAbsolutePath();
    }

    /**
     * Provide the contents of a given resource
     * @param resourceName  the name of the resource, including the project-relative path where it can be found.
     *                     E.g.: if resource is {project}/test/resource/my-file.txt, then "my-file.txt" must be provided.
     * @return the contents (string) of the given resource
     */
    public static String getResourceContents(final String resourceName) {
        try {
            return IOUtils.toString(
                    getResourceUrl(resourceName).openStream(),
                    StandardCharsets.UTF_8
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    public static URL getResourceUrl(String resourceName) {
        return Objects.requireNonNull(TestResourcesHandler.class.getClassLoader().getResource(resourceName));
    }

}
