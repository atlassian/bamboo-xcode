package com.atlassian.bamboo.plugins.xcode.testflight;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.logger.ErrorUpdateHandler;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class TestFlightTask implements CommonTaskType
{
    public static final String TEST_FLIGHT_URL = "http://testflightapp.com/api/builds.json";

    private final ErrorUpdateHandler errorUpdateHandler;

    public TestFlightTask(ErrorUpdateHandler errorUpdateHandler)
    {
        this.errorUpdateHandler = errorUpdateHandler;
    }

    @Override
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException
    {
        return execute(commonTaskContext, TEST_FLIGHT_URL);
    }

    @VisibleForTesting
    TaskResult execute(@NotNull CommonTaskContext commonTaskContext, @NotNull String testFlightUrl) throws TaskException
    {

        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(commonTaskContext);
        final ConfigurationMap config = commonTaskContext.getConfigurationMap();
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();

        final String file = config.get(TestFlightTaskConfigurator.FILE);
        final String apiToken = config.get(TestFlightTaskConfigurator.API_TOKEN);
        final String teamToken = config.get(TestFlightTaskConfigurator.TEAM_TOKEN);
        final String notes = config.get(TestFlightTaskConfigurator.NOTES);
        final boolean notify = config.getAsBoolean(TestFlightTaskConfigurator.NOTIFY);
        final String distributionLists = config.get(TestFlightTaskConfigurator.DISTRIBUTION_LISTS);

        final File pathToFile = new File(commonTaskContext.getWorkingDirectory(), file);

        final HttpPost method = new HttpPost(testFlightUrl);

        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        // IPA file
        if (!pathToFile.exists()) {
            buildLogger.addBuildLogEntry("Could not find specified IPA file '" + pathToFile.getAbsolutePath() + "'");
            return taskResultBuilder.failed().build();
        }
        multipartEntityBuilder.addPart("file", new FileBody(pathToFile, ContentType.DEFAULT_BINARY));

        // DSYM file
        String dsym = config.get(TestFlightTaskConfigurator.DSYM);
        if (StringUtils.isNotEmpty(dsym))
        {
            final File pathToDsym = new File(commonTaskContext.getWorkingDirectory(), dsym);
            if (!pathToFile.exists()) {
                buildLogger.addBuildLogEntry("Could not find specified dSYM file '" + pathToDsym.getAbsolutePath() + "'");
                return taskResultBuilder.failed().build();
            }
            multipartEntityBuilder.addPart("dsym", new FileBody(pathToDsym, ContentType.DEFAULT_BINARY));
        }

        multipartEntityBuilder.addPart("api_token", new StringBody(apiToken, ContentType.MULTIPART_FORM_DATA));
        multipartEntityBuilder.addPart("team_token", new StringBody(teamToken, ContentType.MULTIPART_FORM_DATA));
        multipartEntityBuilder.addPart("notes", new StringBody(notes, ContentType.MULTIPART_FORM_DATA));
        multipartEntityBuilder.addPart("notify", new StringBody(
                Boolean.toString(notify).toLowerCase(),
                ContentType.MULTIPART_FORM_DATA));
        multipartEntityBuilder.addPart("distribution_lists", new StringBody(distributionLists, ContentType.MULTIPART_FORM_DATA));

        final HttpEntity multiPartEntity = multipartEntityBuilder.build();
        method.setEntity(multiPartEntity);

        try(final CloseableHttpClient client = HttpClientBuilder.create().build()) {

            buildLogger.addBuildLogEntry("Uploading '" + pathToFile + "' to TestFlightApp...");
            CloseableHttpResponse response = client.execute(method);

            final int status = response.getStatusLine().getStatusCode();
            if (status == HttpStatus.SC_OK)
            {
                buildLogger.addBuildLogEntry("Upload completed");

                final String body = EntityUtils.toString(response.getEntity());

                try
                {
                    final JSONObject jsonObject = new JSONObject(body);

                    buildLogger.addBuildLogEntry("Bundle version: " + jsonObject.getString("bundle_version"));
                    buildLogger.addBuildLogEntry("Install URL: " + jsonObject.getString("install_url"));
                    buildLogger.addBuildLogEntry("Config URL: " + jsonObject.getString("config_url"));
                    buildLogger.addBuildLogEntry("Created at: " + jsonObject.getString("created_at"));
                    buildLogger.addBuildLogEntry("Device Family: " + jsonObject.getString("device_family"));
                    buildLogger.addBuildLogEntry("Notify team members: " + jsonObject.getString("notify"));
                    buildLogger.addBuildLogEntry("Team: " + jsonObject.getString("team"));
                    buildLogger.addBuildLogEntry("Minimum OS Version: " + jsonObject.getString("minimum_os_version"));
                    buildLogger.addBuildLogEntry("Release Notes: " + jsonObject.getString("release_notes"));
                    buildLogger.addBuildLogEntry("Binary Size: " + jsonObject.getString("binary_size"));

                    taskResultBuilder.success();
                }
                catch (JSONException e)
                {
                    reportError("Could not parse body response as JSON:" + body, e, commonTaskContext);
                    taskResultBuilder.failedWithError();
                }
            }
            else
            {
                buildLogger.addErrorLogEntry("POST response to '" + TEST_FLIGHT_URL + "' was " + status);
                taskResultBuilder.failed();
            }
        } catch (IOException e) {
            reportError("Could not contact TestFlightApp.com: " + e.getMessage(), e, commonTaskContext);
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    private void reportError(String error, Throwable e, CommonTaskContext commonTaskContext)
    {
        errorUpdateHandler.recordError(commonTaskContext.getCommonContext().getResultKey(), "Could not contact TestFlightApp.com: " + e.getMessage(), e);
        commonTaskContext.getBuildLogger().addErrorLogEntry("Could not contact TestFlightApp.com: " + e.getMessage(), e);
    }
}
