package com.atlassian.bamboo.plugins.xcode.hockeyapp;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.xcode.hockeyapp.CountingMultipartEntity.ProgressListener;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class UploadTask implements CommonTaskType {
    private static final String HOCKEY_URL = "https://rink.hockeyapp.net/api/2/apps/upload";

    @Override
    public TaskResult execute(@NotNull CommonTaskContext commonTaskContext) throws TaskException {
        return execute(commonTaskContext, HOCKEY_URL);
    }

    @VisibleForTesting
    TaskResult execute(@NotNull CommonTaskContext commonTaskContext, @NotNull String hockeyUrl) throws TaskException {
        final BuildLogger buildLogger = commonTaskContext.getBuildLogger();

        final String apiToken = commonTaskContext.getConfigurationMap().get("apitoken");
        buildLogger.addBuildLogEntry("Using API Token: " + apiToken);

        TaskResultBuilder taskResult = TaskResultBuilder.newBuilder(commonTaskContext);
        HttpPost method = createPostRequest(commonTaskContext, buildLogger, apiToken, hockeyUrl);
        if ((method != null) && (sendPostRequest(method, buildLogger))) {
            taskResult.success();
        } else {
            taskResult.failed();
        }

        return taskResult.build();
    }

    private HttpPost createPostRequest(CommonTaskContext commonTaskContext, BuildLogger buildLogger, String apiToken, String hockeyUrl) {
        final HttpPost httpPost = new HttpPost(hockeyUrl);
        httpPost.addHeader("X-HockeyAppToken", apiToken);

        final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

        final String ipaPath = commonTaskContext.getConfigurationMap().get("ipa");
        if (!addIPAPart(commonTaskContext, multipartEntityBuilder, ipaPath)) {
            buildLogger.addBuildLogEntry("Could not load the specified IPA file '" + ipaPath + "'");
            return null;
        }

        final String dsymPath = commonTaskContext.getConfigurationMap().get("dsym");
        if ((!StringUtils.isEmpty(dsymPath)) && (!addDSYMPart(commonTaskContext, multipartEntityBuilder, dsymPath))) {
            buildLogger.addBuildLogEntry("Could not load the specified dSYM file '" + dsymPath + "'");
            return null;
        }

        final String notes = commonTaskContext.getConfigurationMap().get("notes");
        multipartEntityBuilder.addPart("notes", new StringBody(notes, ContentType.MULTIPART_FORM_DATA));

        final String download = commonTaskContext.getConfigurationMap().get("download");
        multipartEntityBuilder.addPart("status", new StringBody(
                download.equalsIgnoreCase("true") ? "2" : "1",
                ContentType.MULTIPART_FORM_DATA));

        final String tags = commonTaskContext.getConfigurationMap().get("tags");
        if (tags != null && tags.length() > 0) {
            multipartEntityBuilder.addPart("tags", new StringBody(tags, ContentType.MULTIPART_FORM_DATA));
        }

        final String notify = commonTaskContext.getConfigurationMap().get("notify");
        multipartEntityBuilder.addPart("notify", new StringBody(
                notify.equalsIgnoreCase("true") ? "1" : "0",
                ContentType.MULTIPART_FORM_DATA));

        final HttpEntity multiPartEntity = multipartEntityBuilder.build();

        CountingMultipartEntity countingEntity = new CountingMultipartEntity(multiPartEntity, getLogListener(buildLogger, multiPartEntity.getContentLength()));
        httpPost.setEntity(countingEntity);

        return httpPost;
    }

    private Boolean sendPostRequest(HttpPost post, BuildLogger buildLogger) {
        try(final CloseableHttpClient client = HttpClientBuilder.create().build()) {
            buildLogger.addBuildLogEntry("Uploading build to HockeyApp...");
            CloseableHttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == 201) {
                buildLogger.addBuildLogEntry("Upload completed!");
                return true;
            } else {
                final String body = EntityUtils.toString(response.getEntity());
                buildLogger.addErrorLogEntry("Upload failed with response:");
                buildLogger.addErrorLogEntry(body);
                return false;
            }
        } catch (IOException e) {
            buildLogger.addErrorLogEntry("Upload failed with IOException: " + e.getLocalizedMessage());
            return false;
        }
    }

    private ProgressListener getLogListener(final BuildLogger buildLogger, final long totalSize) {
        return new ProgressListener() {
            private long lastSize = 0;

            public void transferred(long size) {
                if ((long) ((double) lastSize / (double) totalSize * 10.0) < (long) ((double) size / (double) totalSize * 10.0)) {
                    buildLogger.addBuildLogEntry("Upload progress: " + (long) ((double) size / (double) totalSize * 100) + "%");
                }

                lastSize = size;
            }
        };
    }

    private boolean addDSYMPart(CommonTaskContext commonTaskContext, MultipartEntityBuilder multipartEntityBuilder, String dsymPath) {
        final File dsymFile = new File(commonTaskContext.getWorkingDirectory(), dsymPath);
        if (!dsymFile.exists()) {
            return false;
        }
        final FileBody fileBody = new FileBody(dsymFile, ContentType.DEFAULT_BINARY);
        multipartEntityBuilder.addPart("dsym", fileBody);
        return true;
    }

    private Boolean addIPAPart(CommonTaskContext commonTaskContext, MultipartEntityBuilder multipartEntityBuilder, String ipaPath) {
        final File ipaFile = new File(ipaPath);
        if (!ipaFile.exists()) {
            return false;
        }
        final FileBody fileBody = new FileBody(ipaFile, ContentType.DEFAULT_BINARY);
        multipartEntityBuilder.addPart("ipa", fileBody);
        return true;
    }
}
