package com.atlassian.bamboo.plugins.xcode.hockeyapp;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Wrapper around an {@link HttpEntity} that adds monitoring (counting transferred bytes) via
 * provided {@link com.atlassian.bamboo.plugins.xcode.hockeyapp.ProgressListener}.
 */
public class CountingMultipartEntity implements HttpEntity {
  private final HttpEntity entity;
  private final ProgressListener listener;

  public CountingMultipartEntity(final HttpEntity entity, final ProgressListener listener) {
    super();
    this.entity = entity;
    this.listener = listener;
  }

  public long getContentLength() {
    return this.entity.getContentLength();
  }

  @Override
  public Header getContentType() {
    return this.entity.getContentType();
  }

  @Override
  public Header getContentEncoding() {
    return this.entity.getContentEncoding();
  }

  @Override
  public InputStream getContent() throws IOException, IllegalStateException {
    return this.entity.getContent();
  }

  @Override
  public void writeTo(OutputStream outputStream) throws IOException {
    this.entity.writeTo(new CountingOutputStream(outputStream, this.listener));
  }

  @Override
  public boolean isStreaming() {
    return this.entity.isStreaming();
  }

  @Override
  @Deprecated
  public void consumeContent() throws IOException {
    this.entity.consumeContent();
  }

  @Override
  public boolean isRepeatable() {
    return this.entity.isRepeatable();
  }

  @Override
  public boolean isChunked() {
    return this.entity.isChunked();
  }

  public static interface ProgressListener {
    void transferred(long num);
  }

  public static class CountingOutputStream extends FilterOutputStream {
    private final ProgressListener listener;
    private long transferred;

    public CountingOutputStream(final OutputStream out, final ProgressListener listener) {
      super(out);
      this.listener = listener;
      this.transferred = 0;
    }

    public void write(byte[] b, int off, int len) throws IOException {
      out.write(b, off, len);
      this.transferred += len;
      this.listener.transferred(this.transferred);
    }

    public void write(int b) throws IOException {
      out.write(b);
      this.transferred++;
      this.listener.transferred(this.transferred);
    }
  }
}